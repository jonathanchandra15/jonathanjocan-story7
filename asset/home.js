$(document).ready(function(){
    $('.container-accord > .paragraph').hide();

    $('.container-accord > .kotak').click(function(){ 
        $(this).addClass("clicked");
    
        $('.container-accord > .kotak').each(function (){
            var ini = $(this);
            if(ini.hasClass('clicked') == false) {
                ini.next().slideUp();
                ini.addClass('ketutup').removeClass('kebuka');
            }
            else {
                ini.next().slideDown();
                ini.removeClass('clicked');
            }
        })

        if($(this).hasClass('kebuka')) {
            $(this).next().slideUp();
            $(this).removeClass('kebuka').addClass('ketutup');
        }
        else {
            $(this).next().slideDown();
            $(this).removeClass('ketutup').addClass('kebuka');
        }
    })

    $('.toggle-btn').click(function(){
        var ini = $(this);
        if(ini.hasClass('active')) {
            ini.removeClass('active')
            ini.children().children().addClass('round').removeClass('fa-moon');
            if($('html').attr('data-theme') == 'light') {
                trans();
                document.documentElement.setAttribute('data-theme', 'dark');
            }
            else {
                trans();
                document.documentElement.setAttribute('data-theme', 'light');
            }
        }
        else {
            ini.addClass('active');
            ini.children().children().addClass('fa-moon').removeClass('round');
            if($('html').attr('data-theme') == 'light') {
                trans();
                document.documentElement.setAttribute('data-theme', 'dark');
            }
            else {
                trans();
                document.documentElement.setAttribute('data-theme', 'light');
            }
        }
    })
})

let trans = () => {
    document.documentElement.classList.add('transition');
    window.setTimeout(() => {
        document.documentElement.classList.remove('transition');
    }, 600)
}


