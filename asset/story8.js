$(document).ready(() => {

    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=jocan',
        success: function(response) {
            $('#rslt-table').empty();
            if(response.items !== undefined) {
                $('#rslt-table').append('<tr class="header"></tr>');
                $('.header').append('<th>No</th>');
                $('.header').append('<th>Cover</th>');
                $('.header').append('<th>Title</th>');
                $('.header').append('<th>Author</th>');
                for(let i = 0; i < response.items.length; i++) {
                    $('#rslt-table').append('<tr class="isi"></tr>');
                    var no = i + 1;
                    $('.isi').append('<td>' + no + '</td>');
                    if(response.items[i].volumeInfo.imageLinks !== undefined) {
                        if(response.items[i].volumeInfo.imageLinks.thumbnail !== undefined) {
                            $('.isi').append('<td><img src=' + response.items[i].volumeInfo.imageLinks.thumbnail + '></img></td>');
                        }
                        else {
                            $('.isi').append('<td>No Image</td>');
                        }
                    }
                    else {
                        $('.isi').append('<td>No Image</td>');
                    }
                    $('.isi').append('<td>' + response.items[i].volumeInfo.title + '</td>');
                    $('.isi').append('<td class="author"></td>');
                    $('.author').append('<div class="content-author"></div>');
                    if(response.items[i].volumeInfo.authors !== undefined ) {
                        for(let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                            $('.content-author').append('<p>' + response.items[i].volumeInfo.authors[j] + '</p>');
                        }
                    } 
                    else {
                        $('.content-author').append('<p>No Author</p>');
                    }
                    $('.author').removeClass('author').addClass('penulis');
                    $('.content-author').addClass('isi-author').removeClass('content-author');
                    $('.isi').addClass('content').removeClass('isi');
                }
            }
            else{
                $('#result').append('<h1 id="gadahasil">No Result</h1>');
            }
        }
    })    

    $('#button').click(function(){
        var key = $('#search').val();

        if(key == "") {
            alert("Please enter a title");
        }
        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('#rslt-table').empty();
                if(response.items !== undefined) {
                    $('#rslt-table').append('<tr class="header"></tr>');
                    $('.header').append('<th>No</th>');
                    $('.header').append('<th>Cover</th>');
                    $('.header').append('<th>Title</th>');
                    $('.header').append('<th>Author</th>');
                    for(let i = 0; i < response.items.length; i++) {
                        $('#rslt-table').append('<tr class="isi"></tr>');
                        var no = i + 1;
                        $('.isi').append('<td>' + no + '</td>');
                        if(response.items[i].volumeInfo.imageLinks !== undefined) {
                            if(response.items[i].volumeInfo.imageLinks.thumbnail !== undefined) {
                                $('.isi').append('<td><img src=' + response.items[i].volumeInfo.imageLinks.thumbnail + '></img></td>');
                            }
                            else {
                                $('.isi').append('<td>No Image</td>');
                            }
                        }
                        else {
                            $('.isi').append('<td>No Image</td>');
                        }
                        $('.isi').append('<td>' + response.items[i].volumeInfo.title + '</td>');
                        $('.isi').append('<td class="author"></td>');
                        $('.author').append('<div class="content-author"></div>');
                        if(response.items[i].volumeInfo.authors !== undefined ) {
                            for(let j = 0; j < response.items[i].volumeInfo.authors.length; j++) {
                                $('.content-author').append('<p>' + response.items[i].volumeInfo.authors[j] + '</p>');
                            }
                        } 
                        else {
                            $('.content-author').append('<p>No Author</p>');
                        }
                        $('.author').removeClass('author').addClass('penulis');
                        $('.content-author').addClass('isi-author').removeClass('content-author');
                        $('.isi').addClass('content').removeClass('isi');
                    }
                }
                else{
                    $('#result').append('<h1 id="gadahasil">No Result</h1>');
                }
            }
        })
    })
})