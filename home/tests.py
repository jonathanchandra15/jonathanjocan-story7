from django.test import TestCase, Client
from django.urls import resolve
from home import views
from jonathanjocan.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import os
import time

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_benar(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_slash_home(self):
        response = c.get('/home/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_home(self):
        response = resolve('/')
        self.assertEqual(response.func, views.home)

    def test_apakah_pake_template_home_html(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_apakah_ada_tulisan_Jocan(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("Jocan", content)

    def test_apakah_ada_tulisan_Halo_lagi(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("Halo lagi di web Jocan", content)
    
    def test_apakah_ada_toggle_theme_switch(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("<div class='toggle-btn", content)

    def test_apakah_ada_accordion(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn("<div class='accordion'", content)
        self.assertIn("Aktivitas Jocan", content)
        self.assertIn("Pengalaman Kepanitiaan/Organisasi", content)
        self.assertIn("Prestasi Jocan", content)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()

    def test_apakah_toggle_theme_switch_bisa_apa_engga(self):
        self.selenium.get(self.live_server_url)

        html = self.selenium.find_element_by_tag_name('html')
        attr_html = html.get_attribute('data-theme')
        self.assertEqual('light', attr_html)

        toggle_btn = self.selenium.find_element_by_class_name("toggle-btn")

        time.sleep(1)
        toggle_btn.click()
        time.sleep(1)

        self.assertEqual('dark', html.get_attribute('data-theme'))

        time.sleep(1)
        toggle_btn.click()
        time.sleep(1)

        self.assertEqual('light', html.get_attribute('data-theme'))

    def test_apakah_accordionnya_bisa_apa_engga(self):
        self.selenium.get(self.live_server_url)

        accord = self.selenium.find_element_by_name('aktivitas')
        self.assertIn('<div class="paragraph" name="isi-aktivitas" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-pengalaman" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-prestasi" style="display: none', self.selenium.page_source)
        time.sleep(1)
        accord.click()
        time.sleep(1)
        self.assertNotIn('<div class="paragraph" name="isi-aktivitas" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-pengalaman" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-prestasi" style="display: none', self.selenium.page_source)

        accord2 = self.selenium.find_element_by_name('pengalaman')
        time.sleep(1)
        accord2.click()
        time.sleep(1)
        self.assertIn('<div class="paragraph" name="isi-aktivitas" style="display: none', self.selenium.page_source)
        self.assertNotIn('<div class="paragraph" name="isi-pengalaman" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-prestasi" style="display: none', self.selenium.page_source)

        accord3 = self.selenium.find_element_by_name('prestasi')
        time.sleep(1)
        accord3.click()
        time.sleep(1)
        self.assertIn('<div class="paragraph" name="isi-aktivitas" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-pengalaman" style="display: none', self.selenium.page_source)
        self.assertNotIn('<div class="paragraph" name="isi-prestasi" style="display: none', self.selenium.page_source)

        time.sleep(1)
        accord3.click()
        time.sleep(1)
        self.assertIn('<div class="paragraph" name="isi-aktivitas" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-pengalaman" style="display: none', self.selenium.page_source)
        self.assertIn('<div class="paragraph" name="isi-prestasi" style="display: none', self.selenium.page_source)
