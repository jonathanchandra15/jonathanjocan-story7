from django.test import TestCase, Client
from django.urls import resolve
from story8 import views
from jonathanjocan.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
import os
import time

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_benar(self):
        response = c.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_tidak_ada_url_slash_story_8(self):
        response = c.get('/story-8/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_home(self):
        response = resolve('/story8/')
        self.assertEqual(response.func, views.story8Home)

    def test_apakah_pake_template_home_html(self):
        response = c.get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

    def test_apakah_ada_tulisan_Halo_lagi(self):
        response = c.get('/story8/')
        content = response.content.decode('utf8')
        self.assertIn("Halo lagi di web Jocan", content)
    
    def test_apakah_ada_search_box_dengan_tulisan_Search_Title(self):
        response = c.get('/story8/')
        content = response.content.decode('utf8')
        self.assertIn('<input type="text" placeholder="Search Title"', content)

    def test_apakah_ada_button_untuk_search(self):
        response = c.get('/story8/')
        content = response.content.decode('utf8')
        self.assertIn('<div id="button"', content)

    def test_apakah_ada_table_hasil_pencarian(self):
        response = c.get('/story8/')
        content = response.content.decode('utf8')
        self.assertIn('<table', content)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()

    def test_apakah_fungsi_search_bekerja_atau_tidak(self):
        self.selenium.get(self.live_server_url + '/story8/')

        box = self.selenium.find_element_by_id('search')
        box.send_keys('pokemon')
        time.sleep(2)
        button= self.selenium.find_element_by_id('button')
        button.click()

        time.sleep(2)
        self.assertIn('<tr', self.selenium.page_source)
        self.assertIn('<td>Pokemon', self.selenium.page_source)
