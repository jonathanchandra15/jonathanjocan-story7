from django.shortcuts import render
import requests
from django.http import JsonResponse

# Create your views here.
def story8Home(request) :
    return render(request, 'story8.html')