from django.test import TestCase, Client
from django.urls import resolve
from story9 import views
from jonathanjocan.settings import BASE_DIR
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase
from django.contrib.auth.models import User
import os
import time

c = Client()

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_story9_benar(self):
        response = c.get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_url_story9_slash_login_benar(self):
        response = c.get('/story9/login')
        self.assertEqual(response.status_code, 200)

    def test_apakah_ada_url_story9_slash_logout_benar(self):
        response = c.get('/story9/logout')
        self.assertEqual(response.status_code, 302)

    def test_apakah_tidak_ada_url_slash_story_9(self):
        response = c.get('/story-9/')
        self.assertEqual(response.status_code, 404)

    def test_apakah_pake_fungsi_account(self):
        response = resolve('/story9/')
        self.assertEqual(response.func, views.account)

    def test_apakah_pake_fungsi_login(self):
        response = resolve('/story9/login')
        self.assertEqual(response.func, views.log_in)

    def test_apakah_pake_fungsi_logout(self):
        response = resolve('/story9/logout')
        self.assertEqual(response.func, views.log_out)

    def test_apakah_pake_template_account_html(self):
        response = c.get('/story9/')
        self.assertTemplateUsed(response, 'account.html')

    def test_apakah_ada_tulisan_Halo_lagi(self):
        response = c.get('/story9/')
        content = response.content.decode('utf8')
        self.assertIn("Halo lagi di web Jocan", content)
    
    def test_apakah_ada_form_log_in(self):
        response = c.get('/story9/')
        content = response.content.decode('utf8')
        self.assertIn('<label>Username', content)
        self.assertIn('<input type="text"', content)
        self.assertIn('<label>Password', content)
        self.assertIn('<input type="password"', content)

    def test_apakah_ada_button_untuk_log_in_dengan_tulisan_Log_in(self):
        response = c.get('/story9/')
        content = response.content.decode('utf8')
        self.assertIn('<input type="submit" value="Log in"', content)

    def test_apakah_ada_tulisan_log_in_dulu_kalo_langsung_ke_slash_log_in_tapi_belom_log_in(self):
        response = c.get('/story9/login')
        content = response.content.decode('utf8')
        self.assertIn('You are not logged in.', content)

class FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)
        User.objects.create_user('coba', '' , 'coba')
        super(FuncTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FuncTest, self).tearDown()

    def test_apakah_fungsi_log_in_dan_log_out_bekerja_atau_tidak(self):
        self.selenium.get(self.live_server_url + '/story9/')

        username = self.selenium.find_element_by_id('id_username')
        username.send_keys('coba')
        password= self.selenium.find_element_by_id('id_password')
        password.send_keys('coba')

        button = self.selenium.find_element_by_class_name('button')
        button.click()

        time.sleep(2)
        self.assertIn('apa kabar coba', self.selenium.page_source)
        self.assertIn('Log out', self.selenium.page_source)

        #---------------------------------------------------------
        #untuk test Log Out

        logout = self.selenium.find_element_by_class_name('logout')
        logout.click()

        time.sleep(2)

        self.assertNotIn('coba', self.selenium.page_source)

    def test_kalo_belom_login_trus_ke_slash_login_trus_baru_login_trus_baru_ke_slash_login_lagi(self):
        self.selenium.get(self.live_server_url + '/story9/login')

        self.assertIn("You are not logged in.", self.selenium.page_source)
        login = self.selenium.find_element_by_class_name('login')
        login.click()

        time.sleep(2)

        username = self.selenium.find_element_by_id('id_username')
        password= self.selenium.find_element_by_id('id_password')
        username.send_keys("coba")
        password.send_keys('coba')

        button = self.selenium.find_element_by_class_name('button')
        button.click()

        time.sleep(2)
        self.assertIn('apa kabar coba', self.selenium.page_source)
        self.assertIn('Log out', self.selenium.page_source)

    def test_abis_login_ke_slash_story9_lagi(self):
        self.selenium.get(self.live_server_url + '/story9/')

        username = self.selenium.find_element_by_id('id_username')
        username.send_keys('coba')
        password= self.selenium.find_element_by_id('id_password')
        password.send_keys('coba')

        button = self.selenium.find_element_by_class_name('button')
        button.click()

        time.sleep(2)
        self.assertIn('apa kabar coba', self.selenium.page_source)

        self.selenium.get(self.live_server_url + '/story9/')

        self.assertIn('apa kabar coba', self.selenium.page_source)

